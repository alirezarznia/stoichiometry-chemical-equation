Stoichiometry  given  Chemical equation and balance it ... for example : _H2O+_CO2→_O2+_C6H12O6, convert to
6H2O+6CO2→6O2+1C6H12O6. 
You must give the inputs in this format :
sign    Nm       elementm,1      countm,1      …elementm,N     mcountm,Nm

Explanation:
sign : if be +1 it's goal is that the molecule  is left side the equation .
if be -1 it's goal is that the molecule  is right side the equation .
Nm: is the number of atomes in molecule.
We get  Nm of  atoms
first we get atom name and then we get number of that atom


inputes terminate by 0 0
for example:

+1 2 H 2 O 1

+1 2 C 1 O 2

-1 1 O 2

-1 3 C 6 H 12 O 6

0 0

is : _H2O+_CO2→_O2+_C6H12O6,


output: It prints the coefficient of equations, respectively

for  recent example Will be printed : 6 6 6 1 
