//include <god>
/*                   ___    ____    ___    ___
      /\     |        |    |____|  |         /     /\
     /__\    |        |    |  \    |---     /     /__\
    /    \   |___    _|_   |   \   |___    /__   /    \
*/
#include <bits/stdc++.h>
#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<pii , long long >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-10
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);
typedef long long ll;
typedef long double ld;
using namespace std;
inline ll get_num(char ch){
	if(ch == '-')return 0;
	else if(ch >= 'a' && ch <= 'z'){
		return 1 + (ch-'a');
	}
	else if(ch >= 'A' && ch <= 'Z'){
		return 27 +(ch - 'A');
	}
	else if(ch >= '0' && ch <= '9'){
		return 53 +(ch - '0');
	}
}
 int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
/* int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; */ // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}
}
vector<pair<string , ll> >v[21];
vector<pair<string , ll> >vt[21];
vector<string>ss;
double  arr[21][21];
ll  sz;
ll ff=0;
    ll ee=0;
ll FF(){
    Rep(i , ee){
//        Rep(i ,sz){
//        Rep(j ,ee) cout<<arr[i][j]<<" ";
//        cout<<endl;
//        }
//        cout<<endl;
//        cout<<endl;
        if(arr[i][i] == 0){
            bool bl=false;
            For(j ,i+1 , ee){
                if(arr[j][i]>0){
                Rep(k , ee+1){
                            swap(arr[j][k] , arr[i][k]);
                }
                bl= true;
                break;
                }
            }
            if(!bl) continue;
        }
        Rep(j, ee){
            if(j!= i && arr[j][i]){
                {
                double pp = arr[j][i]/arr[i][i];
                Rep(k ,ee+1) arr[j][k]-=(arr[i][k]*pp);
                }
            }
        }
    }
    return 0;
}
ll GC(ll x, ll y){
    if(y==0) return x;
    return  GC(y, x%y );
}
map<string , bool>mp;
int main(){
    //Test;
    ll n;
    ll mos=0 , man=0;
    while(cin>>n){
        if(n==0)    break;
        ll m ; cin >>m;
        Rep(i ,m){
            string ch ; ll e; cin >>ch>>e;
            if(!mp[ch])
            sz++;
            mp[ch]=true;
            if(n==1)
            v[ee].pb(MP(ch,e));
            else
            vt[ee].pb(MP(ch,e));
        }
        if(n==1) mos++;
        else man++;
        ee++;
    }
        ll ii =0;
        for(auto w : mp){
        string q = w.F;
        Rep(j , mos){
            ll cc=0;
            Rep(k , v[j].size()){
                string f = v[j][k].first ; ll p = v[j][k].second;
                if(f==q) cc+=p;
            }
            arr[ii][j]=cc;
        }
         For(j , mos , ee){
            ll cc=0;
            Rep(k , vt[j].size()){
                string f = vt[j][k].first ; ll p = vt[j][k].second;
                if(f==q) cc+=p;
            }
            arr[ii][j]=-cc;
        }
        ii++;
    }
// Rep(i ,ee){
//        Rep(j ,ee) cout<<arr[i][j]<<" ";
//        cout<<endl;
//    }        cout<<endl;
//        cout<<endl;
    FF();
//            cout<<endl;
//        cout<<endl;
//
// Rep(i ,ee){
//        Rep(j ,ee+1) cout<<arr[i][j]<<" ";
//        cout<<endl;
//    }
//     cout<<endl;
    double anss[100];
    for(int i = ee-1 ; i>=0;i--){
      //  if(i+1 <sz){anss[i]=1 ; continue;}
        double aa= 0;
        Rep(j , ee) if(i!= j ) aa+= (-arr[i][j]*anss[j]);
        aa+=arr[i][ee];
        double tt = (aa)/arr[i][i] ;
        if(abs(arr[i][i])<EPS) anss[i]=1;
        else
        anss[i]=tt;
    }
//      Rep(j , ee) {if(j!= ee-1)cout<<anss[j]<<" ";
//            else cout<<anss[j]<<endl;
//                }
    ld ccp[100];
    For(i ,1, 100000){
        bool ss= false;
        Rep(j ,ee) ccp[j]=anss[j];
       // if(i==36)
        //cout<<"y"<<endl;
        Rep(j ,ee){
            ccp[j]*=i;
            ll dd = round(ccp[j]);
            if(abs(dd-ccp[j])>EPS)
                ss=true;
        }
        if(!ss){
                Rep(j , ee) {if(j!= ee-1)cout<<ccp[j]<<" ";
            else cout<<ccp[j]<<endl;
                }
                break;
        }
    }
//    Rep(i , ee) {if(i!= ee-1)cout<<anss[i]<<" ";
//        else cout<<anss[i]<<endl;
//    }
    return 0;
}
